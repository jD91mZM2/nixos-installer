use std::{env, process::Command};

pub type Error = Box<dyn std::error::Error>;

fn main() -> Result<(), Error> {
    println!("cargo:rerun-if-changed=resources/resources.xml");

    let dir = env::current_dir()?;

    let status = Command::new("glib-compile-resources")
        .arg("resources.xml")
        .current_dir(dir.join("resources"))
        .status()?;

    assert!(status.success());
    Ok(())
}
