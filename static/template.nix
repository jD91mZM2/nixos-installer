# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
  ];

  # Set your time zone.
  time.timeZone = {{ timezone | nix_string }};

  {% if bootloader -%}
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  {% endif -%}

  {% if networking -%}
  # networking.hostName = "nixos"; # Define your hostname. Default: Optain via DHCP
  {% if wireless -%}
  networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant. Default: false

  {% else -%}
  # networking.wireless.enable = true;  # Uncomment to enable wireless support via wpa_supplicant. Default: false

  {% endif -%}

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp7s0.useDHCP = true;
  networking.interfaces.virbr0.useDHCP = true;
  networking.interfaces.virbr0-nic.useDHCP = true;

  {% endif -%}

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    firefox # "with" makes this a shorthand for pkgs.firefox
    vim     # "with" makes this a shorthand for pkgs.vim
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.

  {% if gnupg -%}
  # Enable the GnuPG agent
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
    pinentryFlavor = "gnome3";
  };

  {% endif -%}
  {% if zsh -%}
  # Configure the ZSH shell
  programs.zsh = {
    # Whether to configure zsh using NixOS. Default: false
    enable = true;
    # Enable compatibility with bash's completion system. Default: false
    enableBashCompletion = true;
  };

  {% endif -%}
  {% if fish -%}
  # Whether to configure fish shell using Nix. Default: false
  programs.fish.enable = true;

  {% endif -%}

  # List services that you want to enable:

  {% if firewall -%}
  # Open ports in the firewall.
  networking.firewall = {
    enable = true; # Default: true. Turn to false to disable.
    allowedTCPPorts = [ /* open ports here */ ];
  };

  {% endif -%}

  {% if printing -%}
  # Enable CUPS to print documents.
  services.printing.enable = true;

  {% endif -%}

  {% if sound -%}
  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  {% endif -%}

  {% if xorg -%}
  # Enable the X11 windowing system.
  services.xserver = {
    enable = true; # Default: false
    layout = {{ xlayout | nix_string }}; # Default: us

    # Enable touchpad support.
    libinput.enable = true;
  };

  # Enable the KDE Desktop Environment.
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;

  {% endif -%}

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.{{ username | nix_property }} = {
    isNormalUser = true;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
    {%- if shell and shell != "bash" %}
    shell = pkgs.{{ shell }};
    {%- endif %}
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.09"; # Did you read the comment?
}
