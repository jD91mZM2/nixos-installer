use gtk::{
    prelude::*,
    Orientation,
};
use relm::Widget;
use relm_derive::{widget, Msg};

use std::{
    cell::RefCell,
    process::Command,
    rc::Rc,
};

use super::language::{NixView, Msg as NixMsg};

pub struct Model {
    config: Rc<RefCell<String>>,
}

#[derive(Msg)]
pub enum Msg {
    UpdateConf,
}

#[widget]
impl Widget for Install {
    fn model(config: Rc<RefCell<String>>) -> Model {
        Model {
            config,
        }
    }

    fn update(&mut self, event: Msg) {
        match event {
            Msg::UpdateConf => {
                self.config.emit(NixMsg::SetCode(self.model.config.borrow().clone()));
            },
        }
    }

    fn init_view(&mut self) {
        let child = Command::new("nixos-generate-config")
            .arg("--show-hardware-config")
            .output()
            .expect("failed to run nixos-generate-config");
        self.hw_config.emit(NixMsg::SetCode(String::from_utf8(child.stdout).expect("command gave invalid UTF-8")));
    }

    view! {
        gtk::Box {
            orientation: Orientation::Vertical,
            map => Msg::UpdateConf,

            gtk::Box {
                orientation: Orientation::Horizontal,
                homogeneous: true,

                gtk::Box {
                    orientation: Orientation::Vertical,

                    gtk::Label { text: "config.nix", },
                    #[name = "config"]
                    NixView,
                },
                gtk::Box {
                    orientation: Orientation::Vertical,

                    gtk::Label { text: "hardware-config.nix", },
                    #[name = "hw_config"]
                    NixView,
                },
            },
            gtk::Button {
                label: "Install",
            },
        },
    }
}
