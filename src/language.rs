use gtk::prelude::*;
use relm::Widget;
use relm_derive::{widget, Msg};
use tempfile::TempDir;
use sourceview::{
    LanguageManager,
    LanguageManagerExt,
};

use std::{
    fs,
    iter,
};

thread_local! {
    // For some fucking reason, I can't get LanguageManager to load directly
    // from a GResource. I have wasted way too much time on this already.
    // Let's just load it from a temporary directory, why the fuck not?
    static TEMPDIR: TempDir = {
        let tempdir = tempfile::tempdir().expect("failed to create temporary directory");

        let resource = gio::resources_lookup_data("/nixos-installer/resources/language-specs/nix.lang", gio::ResourceLookupFlags::NONE)
            .expect("resource didn't exist");

        fs::write(tempdir.path().join("nix.lang"), resource).expect("failed to write temporary file");

        tempdir
    };

    // LanguageManager, extended with the Nix language
    pub static LANG_MNGR: LanguageManager = {
        TEMPDIR.with(|tempdir| {
            let tempdir_str = tempdir.path().to_str().expect("temporary file path wasn't UTF-8");

            let mngr = LanguageManager::get_default().expect("no default language manager");

            let original_search_path = mngr.get_search_path();
            let search_path: Vec<&str> = iter::once(tempdir_str)
                .chain(original_search_path.iter().map(|s| &**s))
                .collect();
            mngr.set_search_path(&search_path);

            mngr
        })
    };
}

#[derive(Msg)]
pub enum Msg {
    SetCode(String),
}

#[widget]
impl Widget for NixView {
    fn model() -> () {}

    fn update(&mut self, event: Msg) {
        match event {
            Msg::SetCode(text) => {
                let old_vadjustment = self.scroll_view.get_vadjustment().expect("no vadjustment");

                self.text_view
                    .get_buffer()
                    .expect("textview had no buffer")
                    .set_text(&text);

                let new_vadjustment = self.scroll_view.get_vadjustment().expect("no vadjustment");
                new_vadjustment.set_value(old_vadjustment.get_value());
            },
        }
    }

    view! {
        #[name = "scroll_view"]
        gtk::ScrolledWindow {
            #[name = "text_view"]
            sourceview::View {
                hexpand: true,
                vexpand: true,
                editable: false,
                monospace: true,
                buffer: Some(&sourceview::Buffer::new_with_language(
                    &LANG_MNGR.with(|mngr| {
                        mngr
                            .get_language("nix")
                            .expect("unable to highlight nix")
                    })
                )),
            },
        },
    }
}
