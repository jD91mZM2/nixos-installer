use gtk::prelude::*;
use relm::Widget;
use relm_derive::{widget, Msg};

#[derive(Msg)]
pub enum Msg {
    Values(&'static [&'static str]),
    Active(&'static str),
}

#[widget]
impl Widget for ComboBoxEasy {
    fn model() -> () {}

    fn update(&mut self, event: Msg) {
        match event {
            Msg::Values(texts) => {
                self.combo.remove_all();

                for text in &texts[..] {
                    self.combo.append(Some(&text), text);
                }
            },
            Msg::Active(text) => {
                self.combo.set_active_id(Some(&text));
            },
        }
    }

    view! {
        #[name = "combo"]
        gtk::ComboBoxText {}
    }
}
