use gtk::{
    prelude::*,
    Align,
    Orientation,
};
use relm::Widget;
use relm_derive::{widget, Msg};

use std::process::Command;

#[derive(Msg)]
pub enum Msg {
    OpenGParted,
}

#[widget]
impl Widget for Partition {
    fn model() -> () {}

    fn update(&mut self, event: Msg) {
        match event {
            Msg::OpenGParted => {
                let _ = Command::new("gksudo")
                    .arg("gparted")
                    .spawn();
            },
        }
    }

    view! {
        gtk::Box {
            orientation: Orientation::Vertical,
            valign: Align::Center,
            halign: Align::Center,

            gtk::Label {
                text: "Please format and mount to /mnt the root partition you want to install NixOS to.",
            },
            gtk::Button {
                label: "Open GParted",
                clicked => Msg::OpenGParted,
            },
        },
    }
}
