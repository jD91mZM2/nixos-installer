use gtk::{
    prelude::*,
    Orientation,
};
use glib::GString;
use tera::{Context, Tera};
use relm::Widget;
use relm_derive::{widget, Msg};
use serde_json::value::{Value, to_value};

use std::{
    borrow::Cow,
    cell::RefCell,
    collections::{HashSet, HashMap},
    rc::Rc,
};

use super::{
    combobox::{ComboBoxEasy, Msg::*},
    language::{NixView, Msg as NixMsg},
};

fn nix_string(s: &str) -> String {
    let escaped = s
        .replace("\\", "\\\\")
        .replace("\"", "\\\"")
        .replace("${", "\\${");

    let mut wrapped = String::with_capacity(1 + s.len() + 1);
    wrapped.push('"');
    wrapped.push_str(&escaped);
    wrapped.push('"');
    wrapped
}

fn nix_property(s: &str) -> Cow<str> {
    fn is_valid_first_char(c: char) -> bool {
        match c {
            'a'..='z' => true,
            'A'..='Z' => true,
            '_' => true,
            _ => false,
        }
    }
    fn is_valid_char(c: char) -> bool {
        is_valid_first_char(c) || match c {
            '\'' => true,
            '-' => true,
            _ => false,
        }
    }

    if s.chars().next().map(is_valid_first_char).unwrap_or(false) && s.chars().all(is_valid_char) {
        Cow::Borrowed(s)
    } else {
        Cow::Owned(nix_string(s))
    }
}

#[derive(Msg)]
pub enum Msg {
    SetFlag(&'static str, bool),
    SetUser(GString),
    SetShell(GString),
    SetXlayout(GString),
    SetTimezone(GString),
}

pub struct Model {
    configuration: Rc<RefCell<String>>,

    options: HashSet<&'static str>,
    username: String,
    shell: String,
    xlayout: String,
    timezone: String,
}

impl Model {
    pub fn render(&mut self) -> String {
        let mut ctx = Context::new();
        for &option in &self.options {
            ctx.insert(option, &true);
        }

        ctx.insert("shell", &self.shell);
        ctx.insert("username", &self.username);
        ctx.insert("xlayout", &self.xlayout);
        ctx.insert("timezone", &self.timezone);

        let mut tera = Tera::default();
        tera.register_filter("nix_string", |val: &Value, _args: &HashMap<String, Value>| -> tera::Result<Value> {
            let string = tera::try_get_value!("nix_string", "value", String, val);
            Ok(to_value(nix_string(&string)).unwrap())
        });
        tera.register_filter("nix_property", |val: &Value, _args: &HashMap<String, Value>| -> tera::Result<Value> {
            let string = tera::try_get_value!("nix_property", "value", String, val);
            Ok(to_value(&nix_property(&string)).unwrap())
        });

        let conf = tera.render_str(include_str!("../static/template.nix"), &ctx).expect("tera file shouldn't have errors");

        *self.configuration.borrow_mut() = conf.clone();

        conf
    }
    fn set_flag(&mut self, option: &'static str, active: bool) {
        if active {
            self.options.insert(option);
        } else {
            self.options.remove(&option);
        }
    }
}

#[widget]
impl Widget for Generator {
    fn rerender(&mut self) {
        self.nix_view.emit(NixMsg::SetCode(self.model.render()));
    }

    fn model(configuration: Rc<RefCell<String>>) -> Model {
        Model {
            configuration,

            options: ["bootloader", "networking", "sound", "xorg"].iter().copied().collect(),
            username: String::from("the_nix_guy"),
            shell: String::from("bash"),
            xlayout: String::from("us"),
            timezone: String::from("Europe/Amsterdam"),
        }
    }

    fn update(&mut self, event: Msg) {
        match event {
            Msg::SetFlag(option, active) => {
                self.model.set_flag(option, active);

                if option == "networking" {
                    self.wireless.set_visible(active);
                }
                if option == "xorg" {
                    self.xlayout.set_visible(active);
                }
            },
            Msg::SetUser(user) => {
                // Bypass automatic text set_property of relm
                (&mut self.model).username = user.into();
            },
            Msg::SetShell(shell) => {
                // Bypass automatic text set_property of relm
                (&mut self.model).shell = shell.into();

                self.fish.set_active(self.model.shell == "fish");
                self.zsh.set_active(self.model.shell == "zsh");
            },
            Msg::SetXlayout(layout) => {
                // Bypass automatic text set_property of relm
                (&mut self.model).xlayout = layout.into();
            },
            Msg::SetTimezone(timezone) => {
                // Bypass automatic text set_property of relm
                (&mut self.model).timezone = timezone.into();
            },
        }
        self.rerender();
    }

    fn init_view(&mut self) {
        self.rerender();
    }

    view! {
        gtk::Box {
            orientation: Orientation::Horizontal,

            gtk::Box {
                orientation: Orientation::Vertical,
                spacing: 10,

                gtk::Box {
                    orientation: Orientation::Vertical,

                    gtk::Label { text: "System", },
                    gtk::CheckButton {
                        label: "Systemd Bootloader",
                        active: self.model.options.contains("bootloader"),
                        toggled(me) => Msg::SetFlag("bootloader", me.get_active()),
                    },
                    gtk::CheckButton {
                        label: "Network access",
                        active: self.model.options.contains("networking"),
                        toggled(me) => Msg::SetFlag("networking", me.get_active()),
                    },
                    #[name = "wireless"]
                    gtk::CheckButton {
                        label: "Wireless",
                        active: self.model.options.contains("wireless"),
                        visible: self.model.options.contains("networking"),
                        toggled(me) => Msg::SetFlag("wireless", me.get_active()),
                    },
                    gtk::CheckButton {
                        label: "Firewall",
                        active: self.model.options.contains("firewall"),
                        toggled(me) => Msg::SetFlag("firewall", me.get_active()),
                    },
                    gtk::Label { text: "Timezone", xalign: 0.1, },
                    gtk::Entry {
                        text: &self.model.timezone,
                        property_text_notify(me) => Msg::SetTimezone(me.get_text()),
                    },
                },

                gtk::Box {
                    orientation: Orientation::Vertical,

                    gtk::Label { text: "Programs", },
                    gtk::CheckButton {
                        label: "GnuPG Agent",
                        active: self.model.options.contains("gnupg"),
                        toggled(me) => Msg::SetFlag("gnupg", me.get_active()),
                    },
                    #[name = "fish"]
                    gtk::CheckButton {
                        label: "Fish",
                        active: self.model.options.contains("fish"),
                        toggled(me) => Msg::SetFlag("fish", me.get_active()),
                    },
                    #[name = "zsh"]
                    gtk::CheckButton {
                        label: "Z Shell",
                        active: self.model.options.contains("zsh"),
                        toggled(me) => Msg::SetFlag("zsh", me.get_active()),
                    },
                },

                gtk::Box {
                    orientation: Orientation::Vertical,

                    gtk::Label { text: "Services", },
                    gtk::CheckButton {
                        label: "Printing (CUPS)",
                        active: self.model.options.contains("printing"),
                        toggled(me) => Msg::SetFlag("printing", me.get_active()),
                    },
                    gtk::CheckButton {
                        label: "Sound",
                        active: self.model.options.contains("sound"),
                        toggled(me) => Msg::SetFlag("sound", me.get_active()),
                    },
                    gtk::CheckButton {
                        label: "X11 windowing system",
                        active: self.model.options.contains("xorg"),
                        toggled(me) => Msg::SetFlag("xorg", me.get_active()),
                    },
                    #[name = "xlayout"]
                    gtk::Box {
                        orientation: Orientation::Vertical,
                        visible: self.model.options.contains("xorg"),

                        gtk::Label { text: "Keyboard layout", xalign: 0.1, },
                        gtk::Entry {
                            text: &self.model.xlayout,
                            property_text_notify(me) => Msg::SetXlayout(me.get_text()),
                        }
                    },
                },

                gtk::Box {
                    orientation: Orientation::Vertical,

                    gtk::Label { text: "User", },
                    gtk::Entry {
                        text: &self.model.username,
                        property_text_notify(me) => Msg::SetUser(me.get_text())
                    },
                    ComboBoxEasy {
                        Values: &[
                            "bash",
                            "fish",
                            "zsh",
                        ],
                        Active: "bash",

                        changed(me) => Msg::SetShell(me.get_active_id().unwrap_or_else(|| GString::from("bash"))),
                    },
                },
            },
            #[name = "nix_view"]
            NixView,
        },
    }
}
