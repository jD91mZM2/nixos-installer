use gtk::{
    prelude::*,
    Inhibit,
};
use glib::Bytes;
use gio::Resource;
use relm::Widget;
use relm_derive::{widget, Msg};

use std::{
    cell::RefCell,
    rc::Rc,
};

mod combobox;
mod generator;
mod partition;
mod language;
mod install;

use self::{
    generator::Generator,
    partition::Partition,
    install::Install,
};

#[widget]
impl Widget for Title {
    fn model(param: gtk::Stack) -> gtk::Stack {
        param
    }

    fn update(&mut self, _event: ()) {}

    view! {
        gtk::HeaderBar {
            title: Some("NixOS Graphical Installer"),

            gtk::StackSwitcher {
                stack: Some(self.model),
            }
        }
    }
}

#[derive(Default)]
pub struct StackModel {
    configuration: Rc<RefCell<String>>,
}

#[widget]
impl Widget for Stack {
    fn model() -> StackModel {
        StackModel::default()
    }
    fn update(&mut self, _event: ()) {}

    fn init_view(&mut self) {
        let children = self.stack.get_children();

        self.stack.set_child_title(&children[0], Some("Partitioning"));
        self.stack.set_child_title(&children[1], Some("Configuration"));
        self.stack.set_child_title(&children[2], Some("Installation"));
    }

    view! {
        #[name = "stack"]
        gtk::Stack {
            Partition,
            #[name = "generator"]
            Generator (Rc::clone(&self.model.configuration)),
            #[name = "install"]
            Install (Rc::clone(&self.model.configuration)),
        },
    }
}

pub struct Model {
    header: gtk::HeaderBar,
    stack: gtk::Stack,
}

#[derive(Msg)]
pub enum WinMsg {
    Quit,
}

#[widget]
impl Widget for Win {
    fn model() -> Model {
        // Create main area
        let stack = relm::init::<Stack>(())
            .expect("failed to init stack")
            .widget()
            .clone();

        // Create header
        let header = relm::init::<Title>(stack.clone())
            .expect("failed to init title")
            .widget()
            .clone();

        Model {
            header,
            stack,
        }
    }

    fn update(&mut self, event: WinMsg) {
        match event {
            WinMsg::Quit => gtk::main_quit(),
        }
    }

    fn init_view(&mut self) {
        // Add stack dynamically, since relm can't do that
        self.window.add(&self.model.stack);
        self.window.show_all();
    }

    view! {
        #[name = "window"]
        gtk::Window {
            // Set titlebar to header
            titlebar: Some(&self.model.header),

            delete_event(_, _) => (WinMsg::Quit, Inhibit(false))
        },
    }
}

pub type Error = Box<dyn std::error::Error>;

fn main() -> Result<(), Error> {
    // Register resources
    let resource_data = Bytes::from(&include_bytes!("../resources/resources.gresource")[..]);
    let resource = Resource::from_data(&resource_data).expect("resource data was invalid");
    gio::resources_register(&resource);

    // Start window
    Win::run(()).map_err(|()| Error::from("failed to start GUI"))?;

    Ok(())
}
