# nixos-installer

Just experimenting with something... what if NixOS had a graphical installer for
those trying out the project for the first time? I imagine many people get stuck
in the installer trying to figure out how the NixOS config works, instead of
experimenting once they have a working system they're comfortable with.

Obviously you can't edit Nix in *all its glory* graphically, because it's turing
complete, but if we let users select the most common preferences, and have a
playful configuration that explains stuff as it goes on, maybe that could be
worth something.

## Screenshots

![Partitioning screen](screenshots/0.png)
![Configuration screen](screenshots/1.png)
![Installation screen](screenshots/2.png)
